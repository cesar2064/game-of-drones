'use strict'
/* global describe, it */

const assert = require('assert')

describe('Player Model', () => {
  it('should exist', () => {
    assert(global.app.api.models['Player'])
  })
})
