(function(angular) {

  'use strict';
  /**
   * @ngdoc overview
   * @name module
   * @description the module contains the providers
   */
  angular.module('app.provider', []);

})(angular);