(function(angular) {

  'use strict';
  /**
   * @ngdoc overview
   * @name module
   * @description the module contains the factories
   */
  angular.module('app.factory', []);

})(angular);