(function(angular) {

  'use strict';
  /**
   * @ngdoc overview
   * @name module
   * @description the module contains the controllers
   */
  angular.module('app.controller', []);

})(angular);