(function (module) {
  'use strict'
  /**
   * @ngdoc controller
   * @name app.controllers:RoundsCtrl
   *
   */
  module.controller('RoundsCtrl', [
    'playerFactory',
    'playerService',
    'roundFactory',
    'moveFactory',
    'roundMoveFactory',
    '$location',
    controller
  ]);

  function controller(playerFactory, playerService, roundFactory, moveFactory, roundMoveFactory, $location) {

    var ctrl = this;
    var playersLength = playerFactory.data.length;

    ctrl.moves = moveFactory.data;

    ctrl.rounds = roundFactory.data;

    ctrl.playerMove = ctrl.moves[0];

    ctrl.playerIndex = -1;
    ctrl.roundIndex = -1;
    /**
     * @ngdoc function
     * @name nextPlayer
     * @description this method verifies and evaluates the rounds according the number of players   
     * @return the player object wich is next   
     */
    ctrl.nextPlayer = function () {

      ctrl.playerIndex += 1;

      if (ctrl.playerIndex === playersLength) {
        ctrl.playerIndex = 0;
      }
      var player = playerFactory.data[ctrl.playerIndex];

      return (ctrl.player = player);

    };

    /**
     * @ngdoc function
     * @name nextPlayer
     * @description this method verifies and evaluates the rounds according the number of players 
     * @return the round object wich is next       
     */
    ctrl.nextRound = function () {
      ctrl.roundIndex += 1;
      return (ctrl.round = roundFactory.save(new roundFactory.Round([])));
    };

    //starts with the first values
    ctrl.player = ctrl.nextPlayer();
    ctrl.round = ctrl.nextRound();

    /**
     * @ngdoc function
     * @name ok
     * @description this method verifies and evaluates the rounds according the number of players      
     */
    ctrl.ok = function () {

      var roundMove = new roundMoveFactory.RoundMove(ctrl.player, ctrl.playerMove);

      roundFactory.addRoundMove(ctrl.round, roundMove);

      ctrl.playerMove = ctrl.moves[0];

      ctrl.nextPlayer();

      if (ctrl.playerIndex === 0) {
        roundFactory.setWinner(ctrl.round);
        if (roundFactory.setEmperor(playerFactory.data) != undefined) {
          var emperor = roundFactory.emperor;
          emperor.wins = 1;
          playerService.save(emperor).then(function () {
            $location.path("/emperor");
          });

        } else {
          ctrl.nextRound();
        }

      }

    };

  };
})(angular.module('app.controller'));