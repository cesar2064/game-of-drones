(function (module) {
  'use strict'
  /**
   * @ngdoc controller
   * @name app.controllers:EnterPlayersCtrl
   *
   */
  module.controller('EnterPlayersCtrl', [
    'playerFactory',
    'playerService',
    'CONSTANTS',
    '$location',
    controller
  ]);

  function controller(playerFactory, playerService, CONSTANTS, $location) {

    var ctrl = this;
    
    //getting player results
    playerService.getAll().then(function (results) {
      ctrl.playerResults = results.data;
    });

    ctrl.players = playerFactory.data;

    /**
     * @ngdoc function
     * @name validatePlayerName    
     * @param {Number} index - the player index in the ctrl.players array
     * @description validates the players name on change  
     * @return returns the player name error;  
     */
    ctrl.validatePlayerName = function (index) {
      var player = ctrl.players[index];
      //removing extra spaces
      player.name = player.name.trim();
      var playerError = player.error = {};
      var nameError = playerFactory.isValidName(player);
      playerError.name = nameError;
      return nameError;
    };

    /**
     * @ngdoc function
     * @name cleanErrors
     * @description Cleans all error objects in the players array
     */
    ctrl.cleanErrors = function () {
      for (var i = 0; ctrl.players.length > i; i++) {
        delete ctrl.players[i].error;
      }
    };

    /**
     * @ngdoc function
     * @name start    
     * @description This method validates the player's names and according to that
     */
    ctrl.start = function () {

      var areValid = true;

      ctrl.cleanErrors();

      //Validates player's names
      for (var i = 0; ctrl.players.length > i; i++) {
        if (!ctrl.validatePlayerName(i).valid) {
          areValid = false;
          break;
        }
      }

      //validates if the app can continue to the next view
      if (ctrl.players.length > 0 && areValid) {
        playerFactory.ready = true;
        $location.path('/rounds');
      }

    };
  };
})(angular.module('app.controller'));