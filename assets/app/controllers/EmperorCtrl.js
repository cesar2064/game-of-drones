(function (module) {
  'use strict'
  /**
   * @ngdoc controller
   * @name app.controllers:EmperorCtrl
   *
   */
  module.controller('EmperorCtrl', [    
    'roundFactory',
    '$location',
    controller
  ]);

  function controller(roundFactory, $location) {

    var ctrl = this;
    
    ctrl.emperor = roundFactory.emperor;

    /**
     * @ngdoc function
     * @name playAgain
     * @description this method locates the application in the root view      
     */
    ctrl.playAgain = function(){
      $location.path('/');
    };

  };
})(angular.module('app.controller'));