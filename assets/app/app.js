(function(angular) {

  'use strict';
  /**
   * @ngdoc overview
   * @name module
   * @description the app main module
   */
  angular.module('app', [
    'ngRoute',
    'app.controller',
    'app.factory',
    'app.service',
    'app.provider'
  ]);

})(angular);