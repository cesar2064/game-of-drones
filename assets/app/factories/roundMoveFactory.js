(function (module) {
  'use strict'
  /**
   * @ngdoc service
   * @name app.factory:roundMoveFactory
   * @description Handles and stores the round move data
   */
  module.factory('roundMoveFactory', factory);

  function factory() {

    var RoundMove = function (player, move) {
      this.player = player;
      this.move = move;
    };

    var RoundMoveFactory = function () {
      this.RoundMove = RoundMove;
    };

    return new RoundMoveFactory();
  }
})(angular.module('app.factory'));