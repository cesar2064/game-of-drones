(function (module) {
  'use strict'
  /**
   * @ngdoc service
   * @name app.factory:moveFactory
   * @description Handles the move data.
   */
  module.factory('moveFactory', factory);

  function factory() {

    var moves = [
      {
        name: 'paper',
        kills: 'rock'
      },
      {
        name: 'rock',
        kills: 'scissors'
      },
      {
        name: 'scissors',
        kills: 'paper'
      }
    ];

    var MoveFactory = function () {
      this.data = moves;
    };

    return new MoveFactory();

  }
})(angular.module('app.factory'));