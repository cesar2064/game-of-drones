(function (module) {
  'use strict'
  /**
   * @ngdoc service
   * @name app.factory:playerFactory
   * @description Handles and stores the player data.
   */
  module.factory('playerFactory', factory);

  function factory() {

    var Player = function (name) {
      this.name = name;
    };

    var PlayerFactory = function () {
      this.Player = Player;
      this.data = [];
      this.ready = false;
    };

    /**
     * @ngdoc function
     * @name generate
     * @description this method generates empty players
     * @param {Number} quantity - the number of players for generating    
     */
    PlayerFactory.prototype.generate = function (quantity) {
      for (var i = quantity; i--;) {
        this.save(new this.Player(''));
      }
    };

    /**
     * @ngdoc function
     * @name save
     * @description this method creates and adds a new player to the data array
     * @param {Object} player - the player Object    
     */
    PlayerFactory.prototype.save = function (player) {
      var players = this.data;
      var index = players.index;

      if (index != undefined) {
        return players[index] = player;
      }
      player.index = players.length;
      return players.push(player);
    };

    /**
     * @ngdoc function
     * @name cleanData
     * @description this method clean the players factory    
     */
    PlayerFactory.prototype.cleanData = function () {
      this.data = [];
      this.ready = false;
    };

    /**
     * @ngdoc function
     * @name isValidName
     * @description this method validates the player's name' and returns an
     * object with the validation error key and message
     * @param {Object} player - the player object for validating
     * @return {Object} - Returns an object with error key and message
     */
    PlayerFactory.prototype.isValidName = function (player) {
      var players = this.data;
      var emptyRegex = /.+/gi;

      if (player.name.match(emptyRegex) === null) {
        return {
          valid: false,
          message: 'Player\'s name should not be empty'
        }
      }

      //checking if the other players have the same name      

      for (var i = players.length; i--;) {
        var current = players[i];
        if (player != current && players[i].name === player.name) {
          return {
            valid: false,
            message: 'Player\'s name should be unique'
          }
        }
      }

      return {
        valid: true,
        message: ''
      }
    };

    return new PlayerFactory();
  }
})(angular.module('app.factory'));