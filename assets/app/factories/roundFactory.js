(function (module) {
  'use strict'
  /**
   * @ngdoc service
   * @name app.factory:roundFactory
   * @description Handles and stores the round data
   */
  module.factory('roundFactory', [
    'CONSTANTS',
    factory
  ]);

  function factory(CONSTANTS) {

    var Round = function (roundMoves) {
      this.roundMoves = roundMoves;
      this.winner = undefined;
    };

    var RoundFactory = function () {
      this.data = [];
      this.ready = false;
      this.Round = Round;
      this.emperor = undefined;
    };

    /**
     * @ngdoc function
     * @name save
     * @description this method adds a new round to the data array or updates if it has and index attribute
     * @param {Object} round - the round Object    
     */
    RoundFactory.prototype.save = function (round) {
      var rounds = this.data;
      var index = round.index;

      if (index != undefined) {
        return rounds[index] = round;
      }
      round.index = rounds.length;
      rounds.push(round)
      return round;
    };

    /**
     * @ngdoc function
     * @name addRoundMove
     * @description adds a round move object to a round object
     * @param {Object} round - the round Object    
     * @param {Object} roundMove - the roundMove Object to add 
     */
    RoundFactory.prototype.addRoundMove = function (round, roundMove) {
      round.roundMoves.push(roundMove);
    };

    /**
     * @ngdoc function
     * @name cleanData
     * @description this method clean the rounds factory data    
     */
    RoundFactory.prototype.cleanData = function () {
      this.data = [];
      this.emperor = undefined;
    };

    /**
     * TODO - this function only works with two players, modify it for allowing more than two players
     * @ngdoc function
     * @name whoIsRoundTheWinner
     * @description This method determines who is the winner of the round
     * @param {Object} round - the round Object
     * @return {Object} - the winner, can be an object with draw value with true or the player object    
     */
    RoundFactory.prototype.whoIsRoundWinner = function (round) {
      var winner = {
        draw: true
      };
      var roundMoves = round.roundMoves;
      var roundMove1 = roundMoves[0];
      var roundMove2 = roundMoves[1];

      if (roundMove1.move.kills === roundMove2.move.name) {
        winner = roundMove1.player;
      }
      else if (roundMove2.move.kills === roundMove1.move.name) {
        winner = roundMove2.player;
      }
      return winner;
    };

    /**   
     * @ngdoc function
     * @name setWinner
     * @description This method sets the winner to a round object
     * @param {Object} round - the round Object     
     */
    RoundFactory.prototype.setWinner = function (round) {
      round.winner = this.whoIsRoundWinner(round);    
    };

    /**    
     * @ngdoc function
     * @name whoIsTheConqueror
     * @description This method determines who is the winner of all rounds (The conqueror) 
     * @param {players} - the players array to verify 
     * @param {Object|undefined} - returns the player object if there are a winner, otherwise returns undefined   
     */
    RoundFactory.prototype.whoIsEmperor = function (players) {

      var winnerRounds = CONSTANTS.ROUND.WINNERROUNDS;

      for (var p = players.length; p--;) {
        var player = players[p];
        player.wins = 0;
        for (var r = this.data.length; r--;) {
          var round = this.data[r];
          if (player === round.winner) {
            player.wins += 1;
          }
          if (player.wins === winnerRounds) {
            return player;
          }
        }
      }
    };

    /**    
     * @ngdoc function
     * @name setEmperor
     * @description This method determines who is the winner of all rounds (The conqueror) 
     * @param {players} - the players array to verify 
     * @param {Object|undefined} - returns the player object if there are a winner, otherwise returns undefined   
     */
    RoundFactory.prototype.setEmperor = function (players) {
      return (this.emperor = this.whoIsEmperor(players));
    };

    /**
     * @ngdoc function
     * @name cleanData
     * @description this method clean the rounds data array    
     */
    RoundFactory.prototype.cleanData = function () {
      this.data = [];
    };


    return new RoundFactory();
  }
})(angular.module('app.factory'));