(function (app) {
    'use strict'

    var CONSTANTS = {
        'SERVER': {
            'IP': 'http://localhost',
            'PORT': 3000,
            'APIPATH': '/api/v1',
            'DEFAULTCONTENTTYPE': 'application/json',
            get URL() {
                return this.IP + ':' + this.PORT + this.APIPATH
            }
        },
        get PLAYER() {
            return {
                'QUANTITY': 2,
                'PATHS': {
                    'GETBYNAME': {
                        'URL': this.SERVER.URL + '/player/name/{{name}}',
                        'METHOD': 'GET'
                    },
                    'SAVE': {
                        'URL': this.SERVER.URL + '/player',
                        'METHOD': 'PUT'
                    },
                    'GETALL': {
                        'URL': this.SERVER.URL + '/player',
                        'METHOD': 'GET'
                    }
                }
            }
        },
        'ROUND': {
            'WINNERROUNDS': 3
        }
    };

    /**
     * @ngdoc object
     * @name APP_CONFIG
     * @description
     *   Contains the application config.
     */
    app.constant('CONSTANTS', CONSTANTS);



})(angular.module('app'));