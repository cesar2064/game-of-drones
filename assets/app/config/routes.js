(function (app) {
  'use strict'
  /**
   * @ngdoc overview
   * @name app.config:routes
   * @description
   *   The application routes.
   */
  app.config([
    '$routeProvider',
    routes
  ]);

  function routes($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'enter-players.view.html',
        controller: 'EnterPlayersCtrl as ctrl',
        resolve: {
          check: [
            'playerFactory',
            'CONSTANTS',
            function (playerFactory, CONSTANTS) {
              //Cleaning the players array
              playerFactory.cleanData();
              //generating the default empty players
              playerFactory.generate(CONSTANTS.PLAYER.QUANTITY);
            }]
        }
      })
      .when('/rounds', {
        templateUrl: 'rounds.view.html',
        controller: 'RoundsCtrl as ctrl',
        resolve: {
          check: [
            '$location',
            'playerFactory',
            'roundFactory',
            function ($location, playerFactory, roundFactory) {
              roundFactory.cleanData();
              if (!playerFactory.ready) {
                $location.path('/');
              }
            }]
        }
      })
      .when('/emperor', {
        templateUrl: 'emperor.view.html',
        controller: 'EmperorCtrl as ctrl',
        resolve: {
          check: [
            '$location',
            'roundFactory',
            function ($location, roundFactory) {
            if (roundFactory.emperor === undefined) {
              $location.path('/');
            }
          }]
        }
      })
      .otherwise({
        redirectTo: '/'
      });
  };

})(angular.module('app'));