describe('rounds view', function() {
    
    var urlChanged = function(url) {
        return function() {
            return browser.getCurrentUrl().then(function(actualUrl) {
                return actualUrl.indexOf(url) >= 0;
            });
        };
    };


    describe('First round, with a winner', function() {

        it('Should be the first round', function() {
            expect(element(by.css('h3 strong')).getText()).toContain('Round 1');
        });
        it('Should be the first player', function() {
            expect(element(by.css('div[class="col-md-4 col-md-push-1"] h4 strong')).getText()).toContain('Player 1');
        });

        describe('Player1 clicks on ok with paper option', function() {

            it('Should be the first round', function() {
                element(by.buttonText('OK')).click();
                expect(element(by.css('h3 strong')).getText()).toContain('Round 1');
            });

            it('Should switch to the second player', function() {
                expect(element(by.css('div[class="col-md-4 col-md-push-1"] h4 strong')).getText()).toContain('Player 2');
            });
        });

        describe('Player2 clicks on ok with rock option', function() {

            it('Player 1 Should be the winner of the first round', function() {
                element(by.css('select option:nth-child(2)')).click();
                element(by.buttonText('OK')).click();
                expect(element(by.css('table tbody tr:nth-child(1) td:nth-child(2)')).getText()).toContain('Player 1');
            });
        });

    });

    describe('Second round with tie', function() {

        it('Should be the second round', function() {
            expect(element(by.css('h3 strong')).getText()).toContain('Round 2');
        });

        it('Should be the first player', function() {
            expect(element(by.css('div[class="col-md-4 col-md-push-1"] h4 strong')).getText()).toContain('Player 1');
        });

        describe('Player1 clicks on ok with paper option', function() {
            it('Should switch to second player', function() {
                element(by.buttonText('OK')).click();
                expect(element(by.css('div[class="col-md-4 col-md-push-1"] h4 strong')).getText()).toContain('Player 2');
            });
        });

        describe('Player2 clicks on ok with paper option', function() {

            it('The round should be a draw', function() {
                element(by.buttonText('OK')).click();
                expect(element(by.css('table tbody tr:nth-child(2) td:nth-child(2)')).getText()).toContain('Draw');
            });
        });

    });

    describe('Third round with a winner', function() {

        it('Should be the third round', function() {
            expect(element(by.css('h3 strong')).getText()).toContain('Round 3');
        });

        it('Should be the first player', function() {
            expect(element(by.css('div[class="col-md-4 col-md-push-1"] h4 strong')).getText()).toContain('Player 1');
        });

        describe('Player1 clicks on ok with paper option', function() {
            it('Should switch to second player', function() {
                element(by.buttonText('OK')).click();
                expect(element(by.css('div[class="col-md-4 col-md-push-1"] h4 strong')).getText()).toContain('Player 2');
            });
        });

        describe('Player2 clicks on ok with scissors option', function() {
            it('Player 2 should be the winner', function() {
                element(by.css('select option:nth-child(3)')).click();
                element(by.buttonText('OK')).click();
                expect(element(by.css('table tbody tr:nth-child(3) td:nth-child(2)')).getText()).toContain('Player 2');
            });
        });

    });

    describe('Move to the emperor page', function() {

        it('Should be on the emperor page', function() {
            //player 1 with paper option
            element(by.buttonText('OK')).click();
            //player 2 with rock option
            element(by.css('select option:nth-child(2)')).click();
            element(by.buttonText('OK')).click();
            //player 1 with paper option
            element(by.buttonText('OK')).click();
            //player 2 with rock option
            element(by.css('select option:nth-child(2)')).click();
            element(by.buttonText('OK')).click();

            browser.wait(urlChanged("/#/emperor"), 2000);
        });

    });

})