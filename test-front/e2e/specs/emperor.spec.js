describe('emperor view', function() {

    var urlChanged = function(url) {
        return function() {
            return browser.getCurrentUrl().then(function(actualUrl) {
                return actualUrl.indexOf(url) >= 0;
            });
        };
    };

    it('Player 1 should be the emperor',function(){
      expect(element(by.css('h3 strong')).getText()).toContain('Player 1');
    });

    it('Clicking on play again button should change the page to the main url',function(){
      element(by.buttonText('Play Again')).click();
      browser.wait(urlChanged("/#/"), 2000);
    });
})