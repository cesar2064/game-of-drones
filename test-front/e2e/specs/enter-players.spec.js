describe('enter-players view', function () {

  var playerInputs = element.all(by.model('player.name'))
  var playerInput1 = playerInputs.get(0);
  var playerInput2 = playerInputs.get(1);
  var errorSelector1 = 'div[class="col-md-4 col-md-push-1"] div[class="form-group ng-scope"]:nth-child(2) .has-error';
  var errorSelector2 = 'div[class="col-md-4 col-md-push-1"] div[class="form-group ng-scope"]:nth-child(3) .has-error';

  browser.get('http://localhost:3000');

  var urlChanged = function (url) {
    return function () {
      return browser.getCurrentUrl().then(function (actualUrl) {
        return actualUrl.indexOf(url) >= 0;
      });
    };
  };

  describe('Click on the start button without values', function () {

    it('Should be stay in the main page', function () {
      element(by.buttonText('Start!')).click();
      browser.wait(urlChanged("/#/"), 2000);
    });

  });

  describe('Writing player\'s names', function () {

    it('Should not showing errors when writing a correct player name', function () {
      playerInput1.sendKeys('Player 1');
      expect(element(by.css(errorSelector1)).isPresent()).toBeFalsy();
    });

    it('Should showing an error when writing an invalid player name', function () {
      playerInput1.click().clear().then(function () {
        expect(element(by.css(errorSelector1)).isPresent()).toBeTruthy();
      });
    });

    it('Should showing an error when writing a duplicated player name', function () {
      playerInput1.sendKeys('Player 1');
      playerInput2.sendKeys('Player 1');
      expect(element(by.css(errorSelector2)).isPresent()).toBeTruthy();
    });

  });

  describe('Click on the start button with valid values', function () {
    it('Should go to the rounds page', function () {
      playerInput1.click().clear().then(function () {
        playerInput1.sendKeys('Player 1');
        playerInput2.click().clear().then(function () {
          playerInput2.sendKeys('Player 2');
          element(by.buttonText('Start!')).click();
          browser.wait(urlChanged("/#/rounds"), 2000);
        });
      });
    });

  });

});