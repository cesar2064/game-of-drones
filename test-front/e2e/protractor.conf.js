const TrailsApp = require('trails');
const SpecReporter = require('jasmine-spec-reporter');

exports.config = {
  specs: [    
    'specs/enter-players.spec.js',
    'specs/round.spec.js',
    'specs/emperor.spec.js'
  ],
  rootElement:'body',
  onPrepare: function () {


    // add jasmine spec reporter      
    jasmine.getEnv().addReporter(new SpecReporter({
      displayStacktrace: 'all'
    }));
    //starting the trails application
    global.app = new TrailsApp(require('../../'))
    return global.app.start().catch(global.app.stop)
  },
  onCleanUp: function (exitCode) {
    return global.app.stop()
  }
};