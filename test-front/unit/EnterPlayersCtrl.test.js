describe('EnterPlayersCtrl', function () {


	var controller;

	//faking playerFactory
	var playerFactory = {
		isValidName: function (index) {
			return index
		},
		cleanData: function () { },
		Player: function () { },
		save: function () { }
	};
	//faking route $location
	var $location = {
		path: function () { }
	}


	beforeEach(inject(function ($controller) {
		
		controller = $controller('EnterPlayersCtrl', {
			playerFactory: playerFactory,
			$location: $location
		});

		//faking empty players generation
		controller.players = [{
			name: ''
		}, {
			name: ''
		}, {
			name: ''
		}];

	}));

	describe('validatePlayerName function', function () {
		var errorObject;

		describe('Invalid name', function () {

			//faking name validation
			beforeEach(function () {
				errorObject = {
					valid: false,
					message: 'error'
				};
				spyOn(playerFactory, 'isValidName').and.returnValue(errorObject);
			});
			//cleaning the player error object
			afterEach(function () {
				delete controller.players[0].error;
			});

			it('Should create an error object in the player object', function () {
				controller.validatePlayerName(0);
				expect(controller.players[0].hasOwnProperty('error')).toBeTruthy();
			});

			it('Should create an error object with name key in the player object', function () {
				controller.validatePlayerName(0);
				expect(controller.players[0].error.hasOwnProperty('name')).toBeTruthy();
			});

			it('player.error.name should have the error object', function () {
				controller.validatePlayerName(0);
				expect(controller.players[0].error.name).toEqual(jasmine.objectContaining(errorObject));
			});

		});

		describe('Valid name', function () {

			//faking name validation
			beforeEach(function () {
				errorObject = {
					valid: true,
					message: ''
				};

				spyOn(playerFactory, 'isValidName').and.returnValue(errorObject);
			});
			//cleaning the player error object
			afterEach(function () {
				delete controller.players[0].error;
			});

			it('Should create an error object in the player object', function () {
				controller.validatePlayerName(0);
				expect(controller.players[0].hasOwnProperty('error')).toBeTruthy();
			});

			it('Should create an error object with name key in the player object', function () {
				controller.validatePlayerName(0);
				expect(controller.players[0].error.hasOwnProperty('name')).toBeTruthy();
			});

			it('player.error.name should have the error object', function () {
				controller.validatePlayerName(0);
				expect(controller.players[0].error.name).toEqual(jasmine.objectContaining(errorObject));
			});

		});

	});

	describe('start function', function () {

		beforeEach(function () {
			spyOn($location, 'path');
		});

		describe('All Players pass the validation', function () {
			beforeEach(function () {
				errorObject = {
					valid: true,
					message: ''
				};
				//faking validations
				spyOn(playerFactory, 'isValidName').and.returnValue(errorObject);
			});

			it('Should call the $location.path function just once', function () {
				controller.start();
				expect($location.path.calls.count()).toEqual(1);
			});
		});

		describe('Some of the players don\'t pass the validation', function () {
			beforeEach(function () {
				var index = -1;
				//faking validations
				spyOn(playerFactory, 'isValidName').and.callFake(function () {
					index++;
					if (index == 1) {
						return {
							valid: false,
							message: 'error'
						};
					}
					return {
						valid: true,
						message: ''
					};
				});
			});

			it('Shouldn\'t call the $location.path function', function () {
				controller.start();
				expect($location.path.calls.count()).toEqual(0);
			});
		});


		describe('All players don\'t pass the validation', function () {
			beforeEach(function () {
				errorObject = {
					valid: false,
					message: 'error'
				};
				//faking validations
				spyOn(playerFactory, 'isValidName').and.returnValue(errorObject);
			});

			it('Shouldn\'t call the $location.path function', function () {
				controller.start();
				expect($location.path.calls.count()).toEqual(0);
			});
		});

	});

});