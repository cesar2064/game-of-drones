describe('RoundsCtrl', function () {


	var controller;

	//faking playerFactory
	var playerFactory = {
		data: [{
			name: ''
		}, {
			name: ''
		}, {
			name: ''
		}]
	};

	//faking playerService
	var playerService = {
		isValidName: function (index) {
			return index
		},
		cleanData: function () { },
		Player: function () { },
		save: function () { }
	};

	//faking roundFactory
	var roundFactory = {
		Round: function () {
		},
		save: function () {
			return {}
		},
		data: []
	};

	//faking route $location
	var $location = {
		path: function () { }
	}


	beforeEach(inject(function ($controller) {

		controller = $controller('RoundsCtrl', {
			playerFactory: playerFactory,
			playerService: playerService,
			roundFactory: roundFactory,
			$location: $location
		});

	}));

	describe('nextPlayer function', function () {

		it('should start with the first player', function () {
			expect(controller.player).toBe(playerFactory.data[0]);
		});

		it('Should have the first index', function () {
			expect(controller.playerIndex).toBe(0);
		});

		it('Call again the function should get the next player', function () {
			controller.nextPlayer();
			expect(controller.player).toBe(playerFactory.data[1]);
		});

	});

	describe('nextRound function', function () {

		it('should create a round when controller starts', function () {
			expect(controller.round).not.toBe(undefined);
		});

		it('Should have the first index', function () {
			expect(controller.roundIndex).toBe(0);
		});
	});

});