/**
 * @module server
 *
 * Start up the Trails Application.
 */

'use strict'
const app = require('./')
const TrailsApp = require('trails')
const server = new TrailsApp(app)

process.env.NODE_ENV = process.argv[2] || process.env.NODE_ENV || 'development'

server.start().catch(err => server.stop(err))
