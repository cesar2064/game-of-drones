'use strict'

const Service = require('trails-service')

/**
 * @module PlayerService
 * @description Handles the bussines logic from the Players
 */
module.exports = class PlayerService extends Service {

  /**
   * @function findAll
   * @description finds all players.
   * @return {Promise} returns a sequelize promise
   */
  findAll() {
    return this.app.orm.Player.findAll().then((players) => {
      return new Promise((resolve, reject) => {
        if (players === null) {
          resolve([]);
        } else {
          resolve(players);
        }
      });
    });
  }

  /**
   * @function findByName
   * @description find a player by name.
   * @param {String} name - the player's name 
   * @return {Promise} returns a sequelize promise
   */
  findByName(name) {
    return this.app.orm.Player.findOne({ where: { name } })
  };

  /**
   * @function save
   * @description creates a player if the player name doesn't exist in the db, otherwise updates the player.
   * @param {Object} player - the player object to save 
   * @return {Promise} returns a sequelize promise
   */
  save(player) {

    var that = this;

    return this.findByName(player.name).then(function (playerDb) {   
       
      if (playerDb === null) {
        return that.app.orm.Player.create(player);
      }
      var value = playerDb.dataValues;
      var id = value.id;
      player.wins = value.wins + 1;
      return that.app.orm.Player.update(player,{where:{id}})
    });
  }
}

