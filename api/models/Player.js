'use strict'

const Model = require('trails-model')

/**
 * @module Player
 * @description Handles the Player information
 */
module.exports = class Player extends Model {

  static config(app, Sequelize) {
  }

  static schema(app, Sequelize) {
    return {
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      wins: {
        type: Sequelize.INTEGER,
        allowNull: false
      }
    }
  }
}
