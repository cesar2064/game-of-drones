/**
 * Routes Configuration
 * (trails.config.routes)
 *
 * Configure how routes map to views and controllers.
 *
 * @see http://trailsjs.io/doc/config/routes.js
 */

'use strict'

module.exports = [
  {
    method: ['GET'],
    path: '/api/v1/player',
    handler: 'PlayerController.getAll'
  },
  {
    method: ['GET'],
    path: '/api/v1/player/name/:name',
    handler: 'PlayerController.getByName'
  },
  {
    method: ['PUT', 'POST'],
    path: '/api/v1/player',
    handler: 'PlayerController.save'
  }
]
